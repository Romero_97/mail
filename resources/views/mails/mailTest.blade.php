{{--Formato que tiene los mails que enviamos--}}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <style>
            body,body *:not(html):not(style):not(br):not(tr):not(code) {
                box-sizing: border-box;
                font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif,
                'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';
                position: relative;
            }

            body {
                -webkit-text-size-adjust: none;
                background-color: #ffffff;
                color: #000;
                height: 100%;
                line-height: 1.4;
                margin: 0;
                padding: 0;
                width: 100% !important;
            }

            p,ul,ol,blockquote {
                line-height: 1.4;
                text-align: left;
            }

            a {
                color: #3869d4;
            }

            a img {
                border: none;
            }

            /* Typography */
            h1 {
                color: #3d4852;
                font-size: 18px;
                font-weight: bold;
                margin-top: 0;
                text-align: left;
            }

            h2 {
                font-size: 16px;
                font-weight: bold;
                margin-top: 0;
                text-align: left;
            }

            h3 {
                font-size: 14px;
                font-weight: bold;
                margin-top: 0;
                text-align: left;
            }

            p {
                font-size: 16px;
                line-height: 1.5em;
                margin-top: 0;
                text-align: left;
            }

            p.sub {
                font-size: 12px;
            }

            img {
                max-width: 100%;
            }

            /* Layout */
            .wrapper {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 100%;
                background-color: #AB0A35;
                margin: 0;
                padding: 0;
                width: 100%;
            }

            .content {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 100%;
                margin: 0;
                padding: 0;
                width: 100%;
            }

            /* Header */
            .header {
                padding-bottom: 25px;
                text-align: center;
            }

            .header a {
                color: #3d4852;
                font-size: 19px;
                font-weight: bold;
                text-decoration: none;
            }

            /* Logo */
            .logo {
                height: 75px;
                width: 75px;
            }

            /* Body */
            .body {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 100%;
                background-color: #AB0A35;
                border-bottom: 1px solid #AB0A35;
                border-top: 1px solid #AB0A35;
                margin: 0;
                padding: 0;
                width: 100%;
                color: #000;
            }

            .inner-body {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 570px;
                background-color: #ffffff;
                border-color: #e8e5ef;
                border-radius: 2px;
                border-width: 1px;
                box-shadow: 0 2px 0 rgba(0, 0, 150, 0.025), 2px 4px 0 rgba(0, 0, 150, 0.015);
                margin: auto;
                padding: 0;
                width: 570px;
                color: #000;
            }

            /* Subcopy */

            .subcopy {
                border-top: 1px solid #e8e5ef;
                margin-top: 25px;
                padding-top: 25px;
                color: #000;
            }

            .subcopy p {
                font-size: 14px;
                color: #000;
            }

            /* Footer */
            .footer {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 100%;
                margin: 0 auto;
                padding: 0;
                text-align: center;
                width: 100%;
            }

            .footer p {
                color: #b0adc5;
                font-size: 12px;
                text-align: center;
            }

            .footer a {
                color: #b0adc5;
                text-decoration: underline;
            }

            /* Tables */
            .table table {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 100%;
                margin: 30px auto;
                width: 100%;
            }

            .table th {
                border-bottom: 1px solid #edeff2;
                margin: 0;
                padding-bottom: 8px;
            }

            .table td {
                color: #74787e;
                font-size: 15px;
                line-height: 18px;
                margin: 0;
                padding: 10px 0;
            }

            .content-cell {
                max-width: 100vw;
                padding: 32px;
            }

            /* Buttons */
            .action {
                -premailer-cellpadding: 0;
                -premailer-cellspacing: 0;
                -premailer-width: 100%;
                margin: 30px auto;
                padding: 0;
                text-align: center;
                width: 100%;
            }

            .button {
                -webkit-text-size-adjust: none;
                border-radius: 4px;
                color: #fff;
                display: inline-block;
                overflow: hidden;
                text-decoration: none;
                padding: 15px;
            }

            .button-blue,
            .button-primary {
            background-color: #007bff;
            border-color: #007bff;
            }

            .button-green,
            .button-success {
                background-color: #48bb78;
                border-bottom: 8px solid #48bb78;
                border-left: 18px solid #48bb78;
                border-right: 18px solid #48bb78;
                border-top: 8px solid #48bb78;
            }

            .button-red,
            .button-error {
                background-color: #e53e3e;
                border-bottom: 8px solid #e53e3e;
                border-left: 18px solid #e53e3e;
                border-right: 18px solid #e53e3e;
                border-top: 8px solid #e53e3e;
            }

            /* Panels */

            .panel {
                border-left: #2d3748 solid 4px;
                margin: 21px 0;
            }

            .panel-content {
                background-color: #AB0A35;
                color: #000;
                padding: 16px;
            }

            .panel-content p {
                color: #000;
            }

            .panel-item {
                padding: 0;
            }

            .panel-item p:last-of-type {
                margin-bottom: 0;
                padding-bottom: 0;
            }

            /* Utilities */
            .break-all {
                word-break: break-all;
            }

            @media only screen and (max-width: 600px) {
                .inner-body {
                    width: 100% !important;
                }

                .footer {
                    width: 100% !important;
                }
            }

            @media only screen and (max-width: 500px) {
                .button {
                    width: 100% !important;
                }
            }
            .footer-text{
                color: #74787e !important;
            }

            .my-table th,.my-table td{
                border-top: 0px;
                border-bottom: 0px;
                padding: 0.25rem;
            }

            .verificar-email-button{
                border-color: #AB0A35;
                border-radius: 15px;
                background-color: #AB0A35;
                
                font-weight: bold;
                color: white;
                font-size: 15px;
                min-height: 30px;
                text-decoration: none;
                padding: 8px;
                padding-bottom: 10px;
            }
        </style>
    </head>
    <body>
        <table class="wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td align="center">
                    <table class="content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                        {{-- Header --}}
                        <tr>
                            <td class="header">
                                <img src="" alt="image" width="100%" height="75" style="float: left">
                            </td>
                        </tr>
                        {{-- Body --}}
                        <tr>
                            <td class="body" width="100%" cellpadding="0" cellspacing="0" align="center">
                                <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
                                    <!-- Body content -->
                                    <tr>
                                        <td class="content-cell" style="color: #000 !important;">
                                            {{-- Saludo --}}
                                            <h2>¡Hola!</h2>
                                            {{-- Contenido --}}
                                            <p style="text-align: justify; margin-bottom:0;">
                                                {{ $mensaje }}
                                            </p>
                                            <br>
                                            {{-- Despedida --}}
                                            <p style="text-align: justify; margin-bottom:0;">
                                                <br>
                                                Si no ha solicitado este correo electrónico, por favor ignore este mensaje.
                                            </p>
                                            <br>
                                            <table class="subcopy" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                                                <tr>
                                                <td style="text-align: left">
                                                    <br>
                                                    <p class="content-cell-footer" style="color: #000000 !important;">
                                                        <small><Strong>Aviso: </Strong> este mensaje fue enviado desde una cuenta no monitoreada, por favor no responda este mensaje.</small><br>
                                                    </p>
                                                </td>
                                                </tr>
                                                </table>
                                            <br>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="footer" align="center" width="100%" cellpadding="0" cellspacing="0" role="presentation">
                                    {{-- Footer --}}
                                    <tr>
                                        <td class="content-cell">
                                            <p style="text-align:center; font-size:12px !important; color: #ffffff;">© {{ date('Y') }}</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>