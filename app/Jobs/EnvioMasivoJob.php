<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Solicitante;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;

class EnvioMasivoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        // Para correr los job usa el comando php artisan queue:work
        $mensaje = 'Mails Masivos';
        $solicitantes = Solicitante::get();

        /*
            Para el uso en produccioón las colas de trabajo nosotros hacemos uso del Supervisor.
        */
        
        foreach ($solicitantes as $key => $solicitante) {
            Mail::to($solicitante->mail)->send(new TestMail($mensaje));
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
