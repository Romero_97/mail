<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\EnvioMasivoJob;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Tarea programada todos los días a las 8 de la mañana
        // Para correr tareas usa el comando php artisan schedule:work

        /*
            Para el uso en produccioón de tareas automatizadas nosotros hacemos uso de los Cron Job.
        */
        
        $schedule->job(new EnvioMasivoJob)->dailyAt('08:00');
        
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    /**
     * Pudes usar una zona horario especifica.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return 'America/Cancun';
    }
}
