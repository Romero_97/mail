<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;
use App\Models\Solicitante;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Primero es esencial configurar el .env con los datos que usara el MAILER
// De igual forma el archivo que esta en config\mail.php
Route::get('/', function () {
    $mensaje = 'Esto es un mensaje de prueba';

    // Esta es una forma para enviar el mail
    Mail::to('prueba@gmail.com')->send(new TestMail($mensaje));

    // Se puede hacer uso de cualquier modelo mientras este cuente con un campo de correo electrónica.
    $solicitante = Solicitante::find(1);
    Mail::to($solicitante->mail)->send(new TestMail($mensaje));

    // Igualmente esto lo puedes dejar programado
    // Ve al Archivo que esta en App\Console\Kernel.php
});
